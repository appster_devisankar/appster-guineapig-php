<?php
/**************************************************************************************************************
 *  *  *  *  *  *  *  *  *  *  *   Appster   *  *  *  *  *  *   *   *
 **********************************************************************************************************
 * Filename				 :	Guneapig_api.php
 * Description 		  	 :  This file is responsible for request handling of Pocket Butler
 * External Files called :  guniepig_api_model.php
 * Global Variables	  	 :
 *
 * Modification Log
 * 	Date                	 Author                       		Description
 * ---------------------------------------------------------------------------------------------------------
 * 09-Sep-2014			Devi Sankar kar						Created the file.
 ***************************************************************************************************************/
class Guniepig_api extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('guniepig_api_model');
	}
	public function user_registration()
	{
		$data = $this->guniepig_api_model->user_registration();
		echo $data;
	}
	public function user_login()
	{
		$this->guniepig_api_model->user_login();
	}
	public function forget_pass()
	{
		$this->guniepig_api_model->forget_pass();
	}

	public function updateuserprofile()
	{

	}
	public function getuserprofiledetails()
	{
		$user_id = $this->security->xss_clean($this->input->post('user_id'));
		$user_details = $this->guniepig_api_model->getuserprofiledetails($user_id);
	}
	public function logout()
	{
		$this->guniepig_api_model->logout();
	}
	public function get_service_details(){
		$this->guniepig_api_model->get_service_details();
	}
	public function add_services(){
		$this->guniepig_api_model->add_services();
	}
	public function get_model_by_service_id(){
		$this->guniepig_api_model->get_model_by_service_id();
	}
	public function get_hd_by_service_id(){
		$this->guniepig_api_model->get_hd_by_service_id();
	}
	public function get_hd_details(){
		$this->guniepig_api_model->get_hd_details();
	}
	public function send_invitation(){
		$this->guniepig_api_model->send_invitation();
	}
	public function authenticate_invitation(){
		$this->guniepig_api_model->authenticate_invitation();
	}
	public function list_request_friends(){
		$this->guniepig_api_model->list_request_friends();
	}
	public function list_friends(){
		$this->guniepig_api_model->list_friends();
	}
	public function list_friend_request_sent(){
		$this->guniepig_api_model->list_friend_request_sent();
	}
	public function list_friend_request_received(){
		$this->guniepig_api_model->list_friend_request_received();
	}
	public function update_profile(){
		$this->guniepig_api_model->update_profile();
	}
	public function changepassword(){
		$this->guniepig_api_model->changepassword();
	}
	public function add_opening_hours(){
		$this->guniepig_api_model->add_opening_hours();
	}
	public function change_otp(){
		$this->guniepig_api_model->change_otp();
	}
	public function get_product_list(){
		$this->guniepig_api_model->get_product_list();
	}
	public function purchase_product(){
		$this->guniepig_api_model->purchase_product();
	}
	public function unfriend(){
		$this->guniepig_api_model->unfriend();
	}
	public function update_user_subscription(){
		$this->guniepig_api_model->update_user_subscription();
	}
	public function test_notification(){
		$this->guniepig_api_model->send_notification(5,'Message sent from devi');
	}
	public function encript_password(){
		$this->guniepig_api_model->encript_password();
	}

}