<?php
/**************************************************************************************************************
 *  *  *  *  *  *  *  *  *  *  *   Appster   *  *  *  *  *  *   *   *
 ***********************************************************************************************************
 * Filename				 :	Guniepig_api_model.php
 * Description 		  	 :  This file is responsible for rmanaging apis for Guniepig
 * External Files called :
 * Global Variables	  	 :
 *
 * Modification Log
 * 	Date                	 Author                       		Description
 * ---------------------------------------------------------------------------------------------------------
 * 09-Sep-2014			Devi Sankar Kar						Created the file.
 ***************************************************************************************************************/
class Guniepig_api_model extends CI_Model {
	public function __construct()
	{
		$this->load->model('custom_model');
		$this->load->database();
		$this->user_image = realpath(APPPATH.'../photos/user_image');
		$this->load->helper('url');
	}
	/***************************************************************************************************************/
	public function user_registration()
	{
		$user_type = $this->security->xss_clean($this->input->post('user_type'));
		$user_address = $this->security->xss_clean($this->input->post('user_address'));
		$latitude = $this->security->xss_clean($this->input->post('user_latitude'));
		$longitude = $this->security->xss_clean($this->input->post('user_longitude'));
		$facebook_id = $this->security->xss_clean($this->input->post('facebook_id'));
		// if user is a model
		$first_name = $this->security->xss_clean($this->input->post('user_name'));
		//$last_name = '';
		$gender = $this->security->xss_clean($this->input->post('gender'));
		$radius = $this->security->xss_clean($this->input->post('radius'));
		// if user is a hd
		$hd_name = $this->security->xss_clean($this->input->post('hd_name'));
		$business_name = $this->security->xss_clean($this->input->post('business_name'));
		$business_address = $this->security->xss_clean($this->input->post('business_address'));
		$expt_level = $this->security->xss_clean($this->input->post('exp_level'));
		$opening_hours = $this->input->post('opening_hours');
		// user login details
		$user_email = $this->security->xss_clean($this->input->post('user_email'));
		$user_password = $this->security->xss_clean($this->input->post('user_password'));
		$device_token = $this->security->xss_clean($this->input->post('device_token'));

		////
		if($gender == "Male"){
			$gender_value = 1;
		}else{
			$gender_value = 2;
		}
		// check user login credentials
		$this->db->where('user_email =', $user_email);
		$query = $this->db->get('tbl_users');
		if($query->num_rows() == 0)
		{
			// insert into tbl_users
			$user = Array(
				'user_email' => $user_email,
			  	'user_password' => $user_password,
				'user_type' => $user_type,
				'fb_id' => $facebook_id,
			);
			$this->db->trans_start();
			$this->db->insert('tbl_users',$user);
			$user_id = $this->db->insert_id();
			$this->db->trans_complete();
			// insert into user profile table
			if($user_type == 1){
				$user_dtls = array(
					'user_id' => $user_id,
					'model_fname' => $first_name,
					'model_gender'=> $gender_value,
					'radius'	=> $radius,
					'lat'	=> $latitude,
					'lng'	=> $longitude,
					'model_address' => $user_address,
				);
				$this->db->insert('tbl_model_profile',$user_dtls);
			}
			else if($user_type == 2){
				// insert user details for hair dresser
				$user_dtls = array(
					'hd_id' => $user_id,
					'hd_name' => $hd_name,
					'salon_name' => $business_name,
					'salon_address'=> $business_address,
					'latitude'	=> $latitude,
					'longitude'	=> $longitude,
					'expertise_level' => $expt_level,
				);
				$this->db->insert('tbl_hd_profile',$user_dtls);
				// add opening hours
				$openinghours_array = json_decode($opening_hours);
				if(is_array($openinghours_array) && count($openinghours_array) > 0){
					foreach($openinghours_array as $key => $value){
						foreach($value as $days => $schedule){
							$opening_hrs = array(
								'hd_id' => $user_id,
								'week_day' => $days,
								'start_time' => $openinghours_array[$key]->$days->start_time,
								'end_time' => $openinghours_array[$key]->$days->end_time,
								'is_closed' => $openinghours_array[$key]->$days->is_closed,
							);
							$this->db->insert('tbl_salon_opening_time',$opening_hrs);
						}
					}

				}
			}
			$this->insert_user_device_token($user_id,$device_token);
			// uploading image
			$img = "";
			if(isset($_FILES['userfile']['name']) || $_FILES['userfile']['name'] !="") {
				$path = $this->user_image;
				$thisdir = getcwd();
				$name = $_FILES['userfile']['name'];
				list($txt, $ext) = explode(".", $name);
				$actual_image_name = rand(1,9999).time().substr(str_replace(" ", "_", $txt),strlen($txt)).".".$ext;
				$tmp = $_FILES['userfile']['tmp_name'];
				$img_path = $path.'/'.$actual_image_name;
				if(move_uploaded_file($tmp, $img_path))
				{
					//$img = $path.$actual_image_name;
					$img = $actual_image_name;
				}else{
					echo 'image not uploaded';
				}
			}
			// uploading image complete
			if($img != ""){
				$image = array(
					'user_id' => $user_id,
					'image_path' => $img,
					'is_profile_image' => 1
				);
				$this->db->insert('tbl_image_gallery',$image);
			}
			
			$user_details = $this->getuserprofiledetails($user_id);
			
			

			$user_dtls = array('user_id' => $user_id,'user_type' =>$user_type);
			
			
			$user_dtls['original_image'] = base_url().'photos/user_image/'.$user_details['base_image'];
			$user_dtls['thumb_image'] = $this->get_image_user($user_details['base_image'], 100, 100);
			
			
			
			$suc = array('result' =>array('status'=>"success",'userinfo' => $user_dtls));
			echo  json_encode($suc);
		}else{
			echo '{"result":{"status":"error","message":"Email address already exists."}}';
		}
	}
	/***************************************************************************************************************/
	public function get_service_details(){

		$user_id = $this->input->post('user_id');

		$get_hair_details = $this->get_hair_dtls($user_id);
		$get_service_details = $this->get_service_dtls($user_id);
		$service_details = array('hair_details' => $get_hair_details,'service_details' => $get_service_details);
		$suc = array('result' =>array('status'=>"success",'service_info' => $service_details));
		echo  json_encode($suc);
	}
	/***************************************************************************************************************/
	public function get_hair_dtls($user_id){
		$hair_type = array();
		$query = $this->db->get('tbl_hair_type');
		if($query->num_rows() >  0)
		{
			foreach ($query->result() as $row){

				$is_selected = $this->custom_model->getvalues_two("tbl_user_hair_type","user_id",$user_id,"hair_type",$row->hair_id);

				$hair_dtls = array('hair_id' => $row->hair_id ,'hair_name' => $row->hair_type_name,'is_selected'=>$is_selected);
				if($row->gender_type == 1){
					$hair_type['male'][] = $hair_dtls;
				}else{
					$hair_type['female'][] = $hair_dtls;
				}
			}
		}
		return $hair_type;
	}
	/***************************************************************************************************************/
	public function get_service_dtls($user_id){

		$user_type = $this->custom_model->getvalues("tbl_users","user_id",$user_id,"user_type");
		if($user_type == '1')
		{
			$tbl = "tbl_model_services_profile";
			$find = "model_id";
		}
		else
		{
			$tbl = "tbl_hd_services_profile";
			$find = "hd_id";
		}
		// get parent services
		$this->db->where('parent_id', 0);
		$query = $this->db->get('tbl_hd_service');
		if($query->num_rows() >  0){
			foreach ($query->result() as $row){
				$services_array = array();
				// get services
				$this->db->where('parent_id', $row->service_id);
				$query_services = $this->db->get('tbl_hd_service');
				if($query_services->num_rows() >  0){
					$service_array = array();
					foreach ($query_services->result() as $services){
						$is_selected = $this->custom_model->getvalues_two($tbl,$find,$user_id,"service_id",$services->service_id);

						$services_array[] =  array('service_id' => $services->service_id,'service_name' => $services->service_name,'is_selected'=>$is_selected);
					}
				}
				$is_selected = $this->custom_model->getvalues_two($tbl,$find,$user_id,"service_id",$row->service_id);
				$service_type[] = array('service_id' => $row->service_id,'service_name' => $row->service_name,'sub_services' => $services_array,'is_selected'=>$is_selected);
			}
		}
		return $service_type;
	}
	/***************************************************************************************************************/
	public function getuserprofiledetails($user_id){
		$result = array();
		$this->db->where('user_id =', $user_id);
		$query = $this->db->get('tbl_users');
		$user_details = array();
		if($query->num_rows() > 0)
		{
			$user_array = $query->result_array();
			$user_details['user_id'] = $user_array[0]['user_id'];
			$user_details['user_email'] = $user_array[0]['user_email'];
			//$user_details['user_password'] = $user_array[0]['user_password'];
			$user_details['user_type'] = $user_array[0]['user_type']; //1 = model 2 = hairdresser
			if($user_details['user_type'] == 1){
				$this->db->select('tmp.model_fname,tmp.model_gender,tmp.radius,tmp.lat,tmp.lng,tmp.model_address');
				$this->db->select('tig.image_path');
				$this->db->from('tbl_model_profile as tmp');
				$this->db->where('tmp.user_id', $user_details['user_id']);
				$this->db->join('tbl_image_gallery tig', 'tmp.user_id = tig.user_id AND tig.is_profile_image = 1','left outer');
				$query_details = $this->db->get();
				$detail_array = $query_details->result_array();
				$user_details['name'] = $detail_array[0]['model_fname'];
				$user_details['gender'] = $detail_array[0]['model_gender'];//1 = male 2 = female
				$user_details['radius'] = $detail_array[0]['radius'];
				$user_details['latitude'] = $detail_array[0]['lat'];
				$user_details['longitude'] = $detail_array[0]['lng'];
				$user_details['address'] = $detail_array[0]['model_address'];
				if($detail_array[0]['image_path'] != null || $detail_array[0]['image_path'] != ""){
					$user_details['user_image'] = $this->get_image_user($detail_array[0]['image_path'], 206, 206); //1 = model 2 = hairdresser
					$user_details['base_image'] = $detail_array[0]['image_path'];
				}else{
					$user_details['user_image'] = "";
					$user_details['base_image'] = "";
				}
			}else if($user_details['user_type'] == 2){
				$this->db->select('tbl_hd_profile.hd_name,tbl_hd_profile.salon_name,tbl_hd_profile.salon_address,tbl_hd_profile.expertise_level,tbl_hd_profile.latitude,tbl_hd_profile.longitude,tbl_image_gallery.image_path');
				$this->db->from('tbl_hd_profile');
				$this->db->where('tbl_hd_profile.hd_id', $user_details['user_id']);
				$this->db->join('tbl_image_gallery', 'tbl_image_gallery.user_id = tbl_hd_profile.hd_id  AND tbl_image_gallery.is_profile_image = 1','left');
				$query_details = $this->db->get();
				$detail_array = $query_details->result_array();
				$user_details['name'] = $detail_array[0]['hd_name'];
				$user_details['saloonname'] = $detail_array[0]['salon_name'];
				$user_details['salon_address'] = $detail_array[0]['salon_address'];
				$user_details['expertise_level'] = $detail_array[0]['expertise_level'];
				$user_details['latitude'] = $detail_array[0]['latitude'];
				$user_details['longitude'] = $detail_array[0]['longitude'];
				if($detail_array[0]['image_path'] != null || $detail_array[0]['image_path'] != ""){
					$user_details['user_image'] = $this->get_image_user($detail_array[0]['image_path'], 206, 206); //1 = model 2 = hairdresser
					$user_details['base_image'] = $detail_array[0]['image_path'];
				}else{
					$user_details['user_image'] = "";
					$user_details['base_image'] = "";
				}
				$user_details['profile_image'] = $detail_array[0]['image_path'];
			}
		}
		return $user_details;
	}
	/*********************************************************************************************************************/
	public function get_image_user($image_path,$width,$height){
			
		return  base_url()."timthumb.php?src=".base_url().'photos/user_image/'.$image_path;
	}
	/*********************************************************************************************************************/
	public function add_services(){
		$user_id = $this->input->post('user_id');
		$hair_type_ids = $this->input->post('selected_hairtypes');
		$services = $this->input->post('selected_services');

		$this->add_hair_type($user_id,$hair_type_ids);

		$this->db->where('user_id =', $user_id);
		$query = $this->db->get('tbl_users');
		if($query->num_rows() > 0)
		{
			$user_details = array();
			$user_array = $query->result_array();
			$user_type = $user_array[0]['user_type'];
			// add services for model
			if($user_type == 1){
				$this->db->where('model_id =', $user_id);
				$this->db->delete('tbl_model_services_profile');
				$json_array = explode(",", $services);
				foreach ($json_array as $value){
					$data = array('model_id' => $user_id ,'service_id' => $value);
					$this->db->insert('tbl_model_services_profile',$data);
				}
			}
			// add services for hair dresser
			if($user_type == 2){
				$this->db->where('hd_id =', $user_id);
				$this->db->delete('tbl_hd_services_profile');
				$json_array = explode(",", $services);
				foreach ($json_array as $value){
					$data = array('hd_id' => $user_id ,'service_id' => $value);
					$this->db->insert('tbl_hd_services_profile',$data);
				}
			}
			$data = array('has_services' => "1");
			$this->db->where('user_id', $user_id);
			$this->db->update('tbl_users', $data);
			echo '{"result":{"status":"success","message":"services added successfully."}}';
		}else{
			echo '{"result":{"status":"error","message":"no services found for this user."}}';
		}
	}
	/*********************************************************************************************************************/
	public function add_hair_type($user_id,$hair_type_ids)
	{
		$hair_type_ids = explode(",", $hair_type_ids);
		$this->db->where('user_id =', $user_id);
		$this->db->delete('tbl_user_hair_type');
		foreach ($hair_type_ids as $hair_type_id)
		{
			$this->db->insert('tbl_user_hair_type',array('user_id'=>$user_id,'hair_type'=>$hair_type_id));
		}
	}

	/*********************************************************************************************************************/
	public function get_servicies_user($user_id){
		$user_dtls = $this->getuserprofiledetails($user_id);
		$services_array = array();
		if($user_dtls['user_type'] == 1){
			$this->db->where("model_id =",$user_id);
			$query = $this->db->get('tbl_model_services_profile');
		}else{
			$this->db->where("hd_id =",$user_id);
			$query = $this->db->get('tbl_hd_services_profile');
		}
		foreach ($query->result() as $row){
			$services_array[] =  $row->service_id;
		}
		return $services_array;
	}
	/*********************************************************************************************************************/
	public function get_model_by_service_id(){
		$this->load->helper('url');
		$model_list = array();
		$user_id = $this->input->post('user_id');

		$page = $this->input->post('page');
		$listperpage = 20;
		// start paging related task
		if(isset($page))
		{
			$page = $this->input->post('page');
			if ($page > 1)
			{
				$start_from = ($page-1) * $listperpage;
			}
			else
			{
				$start_from = 0;
			}
		}
		else
		{
			$start_from = 0;
		}
		/////////
		$service_array = $this->get_servicies_user($user_id);
		$service_str = implode(",", $service_array);

		$hd_dtls = $this->getuserprofiledetails($user_id);
		$lat = $hd_dtls['latitude'];
		$lng = $hd_dtls['longitude'];
		//// retrieving total records
		$query_model_total = "SELECT distinct(tbl_model_services_profile.model_id) as user_id,
			(
			    3959 * acos (
			      cos ( radians($lat) )
			      * cos( radians( tbl_model_profile.lat) )
			      * cos( radians( tbl_model_profile.lng ) - radians($lng) )
			      + sin ( radians($lat) )
			      * sin( radians( tbl_model_profile.lat ) )
		    		)
		  	) AS distance
			FROM tbl_model_profile , tbl_model_services_profile 
			where tbl_model_profile.user_id = tbl_model_services_profile.model_id
			and tbl_model_services_profile.service_id in ( $service_str ) 
			and
			(
			    3959 * acos (
			      cos ( radians($lat) )
			      * cos( radians( tbl_model_profile.lat ) )
			      * cos( radians( tbl_model_profile.lng ) - radians($lng) )
			      + sin ( radians($lat) )
			      * sin( radians( tbl_model_profile.lat ) )
		    		)
		  	) <=  tbl_model_profile.radius order by distance ";
		$query_total = $this->db->query($query_model_total);
		if($query_total->num_rows() > 0)
		{
			$total_record = $query_total->num_rows();
			$total_pages = ceil($total_record/$listperpage);
		}
		$query_model = $query_model_total." LIMIT $start_from , $listperpage";
		$query = $this->db->query($query_model);
		if($query->num_rows() >  0){
			foreach ($query->result() as $row){
				$invitation_status = 0;
				$status = $this->get_invitation_status($user_id, $row->user_id);
				if(is_array($status) && count($status > 0)){
					if($status['request_status'] == 2){
						$invitation_status = 4;
					}else if($status['request_status'] == 1){
						$invitation_status = 3;
					}else if($status['request_status'] == 0){
						if(($status['request_from'] == $user_id ) && ($status['request_to'] == $row->user_id)){
							$invitation_status = 1;
						}else if(($status['request_from'] == $row->user_id ) && ($status['request_to'] == $user_id)){
							$invitation_status = 2;
						}
					}
				}
					
				$user_details = array();
				$user_details = $this->getuserprofiledetails($row->user_id);
				$user_details['original_image'] = base_url().'photos/user_image/'.$user_details['base_image'];
				$user_details['thumb_image'] = $this->get_image_user($user_details['base_image'], 100, 100);
				$user_details['distance'] = $row->distance;
				$user_details['services'] = $this->get_model_services($row->user_id);
				$user_details['invitation_status'] = $invitation_status; // 0 = no request 1= request sent 2= request received 3 = approved 4 = rejected
				$model_list[] = $user_details;
			}
			$suc = array('result' =>array('status'=>"success",'user_list' => $model_list,'total_pages'=>$total_pages));
			echo  json_encode($suc);
		}else{
			echo '{"result":{"status":"error","message":"no data found."}}';
		}


	}
	/*********************************************************************************************************************/
	public function get_hd_by_service_id(){
		$this->load->helper('url');
		$hd_list = array();
		$user_id = $this->input->post('user_id');
		$page = $this->input->post('page');
		$listperpage = 20;

		$total_pages = "0";


		// start paging related task
		if(isset($page))
		{
			$page = $this->input->post('page');
			if ($page > 1)
			{
				$start_from = ($page-1) * $listperpage;
			}
			else
			{
				$start_from = 0;
			}
		}
		else
		{
			$start_from = 0;
		}
		/////////
		$service_array = $this->get_servicies_user($user_id);
		$service_str = implode(",", $service_array);
		$hd_dtls = $this->getuserprofiledetails($user_id);
		$lat = $hd_dtls['latitude'];
		$lng = $hd_dtls['longitude'];
		$radius = $hd_dtls['radius'];
		$query_hd_total = "SELECT distinct(tbl_hd_services_profile.hd_id) as user_id,
			(
			    3959 * acos (
			      cos ( radians($lat) )
			      * cos( radians( tbl_hd_profile.latitude ) )
			      * cos( radians( tbl_hd_profile.longitude ) - radians($lng) )
			      + sin ( radians($lat) )
			      * sin( radians( tbl_hd_profile.latitude ) )
		    		)
		  	) AS distance
			FROM tbl_hd_profile , tbl_hd_services_profile where tbl_hd_profile.hd_id = tbl_hd_services_profile.hd_id 
			and tbl_hd_services_profile.service_id in ( $service_str ) 
			and
			(
			    3959 * acos (
			      cos ( radians($lat) )
			      * cos( radians( tbl_hd_profile.latitude ) )
			      * cos( radians( tbl_hd_profile.longitude ) - radians($lng) )
			      + sin ( radians($lat) )
			      * sin( radians( tbl_hd_profile.latitude ) )
		    		)
		  	) <= $radius order by distance ";
		$query_total = $this->db->query($query_hd_total);
		if($query_total->num_rows() > 0)
		{
			$total_record = $query_total->num_rows();
			$total_pages = ceil($total_record/$listperpage);
		}
		$query_hd = $query_hd_total." LIMIT $start_from , $listperpage";

		$query = $this->db->query($query_hd);
		if($query->num_rows() >  0){
			foreach ($query->result() as $row){
				$invitation_status = "0";
				$status = $this->get_invitation_status($user_id, $row->user_id);
				if(is_array($status) && count($status > 0)){
					if($status['request_status'] == 2){
						$invitation_status = 4;
					}else if($status['request_status'] == 1){
						$invitation_status = 3;
					}else if($status['request_status'] == 0){
						if(($status['request_from'] = $user_id ) && ($status['request_to'] == $row->user_id)){
							$invitation_status = 1;
						}else if(($status['request_from'] = $row->user_id ) && ($status['request_to'] == $user_id)){
							$invitation_status = 2;
						}
					}
				}else{
					$invitation_status = 0;
				}
				$user_details = array();
				$user_details = $this->getuserprofiledetails($row->user_id);
				//$user_details['user_image'] = $this->get_image_user($user_details['profile_image'], 206, 206);

				$user_details['original_image'] = base_url().'photos/user_image/'.$user_details['base_image'];
				$user_details['thumb_image'] = $this->get_image_user($user_details['base_image'], 100, 100);


				$user_details['distance'] = $row->distance;
				$user_details['services'] = $this->get_hd_services($row->user_id);
				$user_details['invitation_status'] = $invitation_status; // 0 = no request 1= request sent 2= request received 3 = approved 4 = rejected


				// hd opening hrs
				$this->db->where('hd_id',$row->user_id);
				$query_opening_hrs = $this->db->get('tbl_salon_opening_time');
				$opening_hrs_array = array();
				if($query_opening_hrs->num_rows() >  0){

					foreach ($query_opening_hrs->result() as $rows){
						$opening_hrs_array[] = array('day' => $rows->week_day,'start_time' => $rows->start_time,'end_time' => $rows->end_time,'is_closed' => $rows->is_closed);
					}
				}
				$user_details['opening_hours'] = $opening_hrs_array;
				$hd_list[] = $user_details;
			}
			$suc = array('result' =>array('status'=>"success",'user_list' => $hd_list ,'total_pages'=>$total_pages));
			echo  json_encode($suc);
		}else{
			echo '{"result":{"status":"error","message":"no data found."}}';
		}
	}
	/*****************************************************************************************************************************/
	public function user_login(){
		$user_email = $this->security->xss_clean($this->input->post('user_email'));
		$user_password = $this->security->xss_clean($this->input->post('user_password'));
		$facebook_id = $this->security->xss_clean($this->input->post('facebook_id'));
		$device_token = $this->security->xss_clean($this->input->post('device_token'));
		if(isset($facebook_id) && $facebook_id != ""){
			$this->db->select("user_id,has_services");
			$this->db->where('fb_id',$facebook_id);
			$query = $this->db->get('tbl_users');
			if($query->num_rows() >  0){
				$user  = $query->result_array();
				$user_id = $user[0]['user_id'];
				$has_service = $user[0]['has_services'];
				$user_dtls = $this->getuserprofiledetails($user_id);




				$user_dtls['has_services'] = $has_service;
				if($user_dtls['user_type'] == 1 && $user_dtls['has_services'] == 1){
					//$user_dtls['services'] = $this->get_model_services($user_id);
					$user_dtls['services'] = $this->get_service_dtls($user_id);
				}else if($user_dtls['user_type'] == 2){
					$user_dtls['opening_hours'] = $this->get_opening_hours($user_id);
					if($user_dtls['has_services'] == 1){
					//	$user_dtls['services'] = $this->get_hd_services($user_id);
					$user_dtls['services'] = $this->get_service_dtls($user_id);
					}
					$user_dtls['subscription_details'] = $this->user_subscription_details($user_id);
				}

				$user_dtls['original_image'] = base_url().'photos/user_image/'.$user_dtls['base_image'];
				$user_dtls['thumb_image'] = $this->get_image_user($user_dtls['base_image'], 100, 100);


				$this->insert_user_device_token($user_id,$device_token);
				$suc = array('result' =>array('status'=>"success",'user_dtls' => $user_dtls));
				echo  json_encode($suc);
			}else{
				echo '{"result":{"status":"error","message":"User does not exist"}}';
			}
		}else{
			$this->db->select("user_id,has_services,is_otp,otp_changed");
			$this->db->where('user_email',$user_email);
			$this->db->where('user_password',$user_password);
			$query = $this->db->get('tbl_users');
			if($query->num_rows() >  0){
				$user  = $query->result_array();
				$is_otp = $user[0]['is_otp'];
				$is_changed = $user[0]['otp_changed'];
				if(($is_otp == 1 && $is_changed == 0) || ($is_otp == 0 && $is_changed == 1)){
					$user_id = $user[0]['user_id'];
					$has_service = $user[0]['has_services'];
					$user_dtls = $this->getuserprofiledetails($user_id);
					$user_dtls['is_otp'] = $is_otp;
					// if one time password provided then
					if($is_otp == 1){
						$data = array(
							'is_otp'	=> '0'
							);
							$this->db->where('user_email', $user_email);
							$this->db->update('tbl_users', $data);
					}
					$user_dtls['has_services'] = $has_service;
					if($user_dtls['user_type'] == 1 && $user_dtls['has_services'] == 1){
					//	$user_dtls['services'] = $this->get_model_services($user_id);
						
						$user_dtls['services'] = $this->get_service_dtls($user_id);
					}else if($user_dtls['user_type'] == 2){
						$user_dtls['opening_hours'] = $this->get_opening_hours($user_id);
						if($user_dtls['has_services'] == 1){
						//	$user_dtls['services'] = $this->get_hd_services($user_id);
						}
						$user_dtls['subscription_details'] = $this->user_subscription_details($user_id);
					}

					$user_dtls['original_image'] = base_url().'photos/user_image/'.$user_dtls['base_image'];
					$user_dtls['thumb_image'] = $this->get_image_user($user_dtls['base_image'], 100, 100);



					$suc = array('result' =>array('status'=>"success",'user_dtls' => $user_dtls));
					echo  json_encode($suc);
				}else{
					echo '{"result":{"status":"error","message":"Invalid credential"}}';
				}
			}else{
				echo '{"result":{"status":"error","message":"Invalid credential"}}';
			}
		}
	}
	/*****************************************************************************************************************************/
	public function get_hd_details(){
		$user_id = $this->input->post('user_id');
		$user_dtls = $this->getuserprofiledetails($user_id);
		if(is_array($user_dtls)){
			//$user_dtls['user_image'] = base_url().'photos/user_image/'.$user_details['profile_image'];


			$user_dtls['original_image'] = base_url().'photos/user_image/'.$user_dtls['base_image'];
			$user_dtls['thumb_image'] = $this->get_image_user($user_dtls['base_image'], 100, 100);



			// hd services details

			$services = $this->get_service_dtls($user_id);



			// hd opening hrs
			$this->db->where('hd_id',$user_id);
			$query_opening_hrs = $this->db->get('tbl_salon_opening_time');
			$opening_hrs_array = array();
			if($query_opening_hrs->num_rows() >  0){
				foreach ($query_opening_hrs->result() as $rows){
					$opening_hrs_array[] = array('day' => $rows->week_day,'start_time' => $rows->start_time,'end_time' => $rows->end_time,'is_closed' => $rows->is_closed);

				}
			}
			$user_dtls['services'] = $services;
			$user_dtls['opening_hours'] = $opening_hrs_array;
			$suc = array('result' =>array('status'=>"success",'user_dtls' => $user_dtls));
			echo  json_encode($suc);
		}else{
			echo '{"result":{"status":"error","message":"User does not exists"}}';
		}
	}
	/*********************************************************************************************************************************/
	public function get_hd_services($hd_id){
		$this->db->select('tbl_hd_services_profile.service_id,tbl_hd_service.service_name');
		$this->db->from('tbl_hd_services_profile');
		$this->db->where('tbl_hd_services_profile.hd_id', $hd_id);
		$this->db->join('tbl_hd_service', 'tbl_hd_services_profile.service_id = tbl_hd_service.service_id');
		$query_services = $this->db->get();
		$services = array();
		if($query_services->num_rows() >  0){
			foreach ($query_services->result() as $row){
				$services[] = array('service_id' => $row->service_id,'service_name' => $row->service_name);
			}
		}
		return $services;
	}
	/*********************************************************************************************************************************/
	public function get_model_services($model_id){
		$this->db->select('tbl_model_services_profile.service_id,tbl_hd_service.service_name');
		$this->db->from('tbl_model_services_profile');
		$this->db->where('tbl_model_services_profile.model_id', $model_id);
		$this->db->join('tbl_hd_service', 'tbl_model_services_profile.service_id = tbl_hd_service.service_id');
		$query_services = $this->db->get();
		//echo $this->db->last_query();exit();
		$services = array();
		if($query_services->num_rows() >  0){
			foreach ($query_services->result() as $row){
				$services[] = array('service_id' => $row->service_id,'service_name' => $row->service_name);
			}
		}
		return $services;
	}
	/*********************************************************************************************************************************/
	public function get_invitation_status($sender,$receiver){
		$request_details = array();
		$this->db->from('tbl_request_contact');
		$where = "(request_from = ".$sender." AND request_to=".$receiver.") OR (request_from = ".$receiver." AND request_to=".$sender.")";
		$this->db->where($where);
		$query = $this->db->get();
		if($query->num_rows() >  0){
			$result  = $query->result_array();
			$request_details['request_id'] = $result[0]['request_id'];
			$request_details['request_from'] = $result[0]['request_from'];
			$request_details['request_to'] = $result[0]['request_to'];
			$request_details['request_status'] = $result[0]['request_status'];
		}
		return $request_details;
	}
	/*********************************************************************************************************************************/
	public function send_invitation(){
		$sender_id = $this->input->post('sender_id');
		$receiver_id = $this->input->post('receiver_id');
		$status = $this->get_invitation_status($sender_id,$receiver_id);
		$get_sender_details = $this->getuserprofiledetails($sender_id);
		if($get_sender_details['user_type'] == 2){
			$subscription_details = $this->user_subscription_details($sender_id);
			if($subscription_details['is_subscribed'] == 0 && $subscription_details['total_request'] >= 2){
				echo '{"result":{"status":"error","message":"not authorised to send more request"}}';
			}
		}
		$update_status = 0;
		if(is_array($status) && count($status) > 0){
			if($status['request_status'] == 2){
				$where = "request_status = 2 and (request_from = ".$sender_id." AND request_to=".$receiver_id.") OR (request_from = ".$receiver_id." AND request_to=".$sender_id.")";
				$this->db->where($where);
				$this->db->delete('tbl_request_contact');
				$update_status = 1;
			}else if($status['request_status'] == 1){
				echo '{"result":{"status":"error","message":"already friend"}}';
			}else if($status['request_status'] == 0){
				if(($status['request_from'] = $sender_id ) && ($status['request_to'] == $receiver_id)){
					echo '{"result":{"status":"error","message":"Requerst alredy sent before"}}';
				}else if(($status['request_from'] = $receiver_id ) && ($status['request_to'] == $sender_id)){
					echo '{"result":{"status":"error","message":"already get a request from this user."}}';
				}
			}
		}else{
			$update_status = 1;
		}
		if($update_status == 1){
			$data = array('request_from' => $sender_id,'request_to' => $receiver_id);
			$this->db->insert('tbl_request_contact',$data);
		}
		if($get_sender_details['user_type'] == 1){
			echo '{"result":{"status":"success","message":"request sent successfully"}}';
		}else{
			$subscription_details = $this->user_subscription_details($sender_id);
			$suc = array("result" =>array("status"=>"success","message" => "request sent successfully" ,"subscription_details" => $subscription_details));
			echo  json_encode($suc);
		}
	}
	/*********************************************************************************************************************************/
	public function authenticate_invitation(){
		$invitation_id = $this->input->post('invitation_id');
		$receiver_id = $this->input->post('user_id');
		$invitation_status = $this->input->post('invitation_status'); // 1 = allow 2= deny
		$this->db->where('request_id',$invitation_id);
		$query = $this->db->get('tbl_request_contact');
		if($query->num_rows() >  0){
			$result  = $query->result_array();
			$receiver = $result[0]['request_to'];
			$request_status = $result[0]['request_status'];
			if(($receiver == $receiver_id) && ($request_status == 0)){
				$data = array('request_status' => $invitation_status);
				$this->db->where('request_id', $invitation_id);
				$this->db->update('tbl_request_contact', $data);
				echo '{"result":{"status":"success","message":"successfully got response from receiver"}}';
			}else{
				echo '{"result":{"status":"error","message":"not authorise to allow/deny request"}}';
			}
		}else{
			echo '{"result":{"status":"error","message":"not a valid request id"}}';
		}
	}
	/*********************************************************************************************************************************/
	public function list_request_friends(){
		$request_list = array();
		$friendlist = array();
		$request_status = 0;
		$user_id = $this->input->post('user_id');
		$page = $this->input->post('page');
		// get friend request list
		$query_total = "SELECT request_id,request_from,request_to from tbl_request_contact where  request_status = 0 and  (request_to = ".$user_id." or request_from = ".$user_id.")";
		$query =  $this->db->query($query_total);
		if($query->num_rows() >  0){
			foreach ($query->result() as $row){
				if($row->request_from == $user_id){
					$request_status = 1; // request sent
				}else{
					$request_status = 2; // request received
				}
				if($row->request_from != $user_id){
					$user_details = $this->getuserprofiledetails($row->request_from);
				}else{
					$user_details = $this->getuserprofiledetails($row->request_to);
				}
				$user_details['profile_image'] = $this->get_image_user($user_details['image_path'], 100, 100);
				if($user_details['user_type'] == 2){
					$user_details['services'] = $this->get_hd_services($user_details['user_id']);
					$user_details['opening_hours'] = $this->get_opening_hours($user_details['user_id']);
				}else{
					$user_details['services'] = $this->get_model_services($user_details['user_id']);
				}
				$user_details['profile_image'] = $this->get_image_user($user_details['image_path'], 100, 100);
				$user_details['request_status'] = $request_status;
				$user_details['invitation_id'] = $row->request_id;
				$request_list[] = $user_details;
			}
		}
		// get friend  list
		$listperpage = 20;
		if(isset($page))
		{
			$page = $this->input->post('page');
			if ($page > 1)
			{
				$start_from = ($page-1) * $listperpage;
			}
			else
			{
				$start_from = 0;
			}
		}
		else
		{
			$start_from = 0;
		}
		$query_total = "SELECT request_id from tbl_request_contact where  request_status = 1 and  (request_to = ".$user_id." or request_from = ".$user_id.")";

		$query_total = $this->db->query($query_total);
		$total_pages = 0;
		if($query_total->num_rows() > 0)
		{
			$total_record = $query_total->num_rows();
			$total_pages = ceil($total_record/$listperpage);
		}
		$friendlist['total_page'] = $total_pages;
		$where = "request_status = 1 and (request_from = ".$user_id."  OR  request_to=".$user_id.")";
		$this->db->where($where);
		$this->db->order_by("request_id", "desc");
		$this->db->limit($listperpage, $start_from);
		$query = $this->db->get('tbl_request_contact');
		if($query->num_rows() >  0){
			foreach ($query->result() as $row){
				if($row->request_from == $user_id){
					$request_status = 1; // request sent
				}else{
					$request_status = 2; // request received
				}
				if($row->request_from != $user_id){
					$user_details = $this->getuserprofiledetails($row->request_from);
				}else{
					$user_details = $this->getuserprofiledetails($row->request_to);
				}
				$user_details['profile_image'] = $this->get_image_user($user_details['image_path'], 100, 100);
				if($user_details['user_type'] == 2){
					$user_details['services'] = $this->get_hd_services($user_details['user_id']);
					$user_details['opening_hours'] = $this->get_opening_hours($user_details['user_id']);
				}else{
					$user_details['services'] = $this->get_model_services($user_details['user_id']);
				}
				//$user_details = $this->getuserprofiledetails($row->request_from);
				$user_details['profile_image'] = $this->get_image_user($user_details['profile_image'], 100, 100);
				$friendlist['user_list'][] = $user_details;
			}
		}
		$suc = array('result' =>array('status'=>"success",'user_list' => array('request_list' => $request_list , 'friend_list' => $friendlist)));
		echo  json_encode($suc);
	}
	/***************************************************************************************************************************************/
	private function insert_user_device_token($user_id,$device_token)
	{
		$this->db->where('user_id =', $user_id);
		$this->db->where('device_token =', $device_token);
		$query = $this->db->get('user_device_token');
		if($query->num_rows() == 0)
		{
			$user = Array(
				'user_id' => $user_id,
			  	'device_token' => $device_token,
				'device_type' => 'iPhone'
				);
				$this->db->insert('user_device_token',$user);
		}
	}
	/***************************************************************************************************************************************/
	public function changepassword()
	{
		$user_id =  $this->input->post('user_id');
		$oldpassword = $this->input->post('old_password');
		$newpassword = $this->input->post('new_password');
		$this->db->where('user_id =', $user_id);
		$this->db->where('user_password =', $oldpassword);
		$query = $this->db->get('tbl_users');
		if($query->num_rows() > 0){
			$data = array('user_password' => $newpassword);
			$this->db->where('user_id =', $user_id);
			if($this->db->update('tbl_users', $data)){
				echo '{"result":{"status":"success","message":"Password updated successfully"}}';
			}else{
				echo '{"result":{"status":"error","message":"Unable to perform this task. Please try again later."}}';
			}
		}else{
			echo '{"result":{"status":"error","message":"Old password is not correct"}}';
		}
	}
	/***************************************************************************************************************************************/
	public function update_profile(){
		$user_id =  $this->input->post('user_id');
		$user_type = $this->input->post('user_type');
		$oldpassword = $this->input->post('old_password');
		$newpassword = $this->input->post('new_password');
		$user_address = $this->security->xss_clean($this->input->post('user_address'));
		$latitude = $this->security->xss_clean($this->input->post('user_latitude'));
		$longitude = $this->security->xss_clean($this->input->post('user_longitude'));
		$username = $this->security->xss_clean($this->input->post('user_name'));
		$exp_level = $this->input->post('exp_level');
		if($user_type == 1){

			$radius = $this->security->xss_clean($this->input->post('radius'));
			if($user_address == "")
			{
				$data = array(
				'model_fname' => $username,
				'radius'	 => $radius

				);
			}
			else
			{
				
				
				$data = array(
				'model_fname' => $username,
				'lat'	=> $latitude,
				'lng'	=> $longitude,
				'model_address' => $user_address,
				'radius'	 => $radius
				);
			}


			$this->db->where('user_id', $user_id);
			$this->db->update('tbl_model_profile', $data);
		}
		else if($user_type == 2){
			$salonname = $this->security->xss_clean($this->input->post('salon_name'));

			if($user_address == "")
			{
				$data = array(
				'hd_name' => $username,
				'salon_name'	=> $salonname,
				'expertise_level' => $exp_level,
				);
			}
			else
			{
				$data = array(
				'hd_name' => $username,
				'latitude'	=> $latitude,
				'longitude'	=> $longitude,
				'salon_address' => $user_address,
				'salon_name'	=> $salonname,
				'expertise_level' => $exp_level,
				);
			}


			$this->db->where('hd_id', $user_id);
			$this->db->update('tbl_hd_profile', $data);
			$opening_hours = $this->input->post('opening_hours');
			if($opening_hours != ""){
				$this->add_opening_hours();
			}
		}
		// update profile photo
		$img		= "";
		if(isset($_FILES['userfile']['name']) || $_FILES['userfile']['name'] !="") {
			// delete existing image
			$this->db->where('user_id =', $user_id);
			$this->db->where('is_profile_image =', 1);
			$query = $this->db->get('tbl_image_gallery');
			if($query->num_rows() > 0){
				$result  = $query->result_array();
				$image_path = $this->user_image.'/'.$result[0]['image_path'];
				if (file_exists($image_path)) {
					unlink($image_path);
				}
				$this->db->where('user_id =', $user_id);
				$this->db->where('is_profile_image =', 1);
				$this->db->delete('tbl_image_gallery');
			}
			// upload new image
			$path = $this->user_image;
			$thisdir = getcwd();
			$name = $_FILES['userfile']['name'];
			list($txt, $ext) = explode(".", $name);
			$actual_image_name = rand(1,9999).time().substr(str_replace(" ", "_", $txt),strlen($txt)).".".$ext;
			$tmp = $_FILES['userfile']['tmp_name'];
			$img_path = $path.'/'.$actual_image_name;
			if(move_uploaded_file($tmp, $img_path))
			{
				$img = $actual_image_name;
			}else{
				echo 'image not uploaded';
			}
		}
		if($img != ""){
			$image = array(
					'user_id' => $user_id,
					'image_path' => $img,
					'is_profile_image' => 1
			);
			$this->db->insert('tbl_image_gallery',$image);
		}
		$user_dtls = $this->getuserprofiledetails($user_id);
		if($user_dtls['user_type'] == 2){
			//$user_dtls['services'] = $this->get_hd_services($user_id);
			
			 //($user_id);
			
			$user_dtls['opening_hours'] = $this->get_opening_hours($user_id);
		}
		
		$user_dtls['services'] = $this->get_service_dtls($user_id);

		$user_dtls['original_image'] = base_url().'photos/user_image/'.$user_dtls['base_image'];
		$user_dtls['thumb_image'] = $this->get_image_user($user_dtls['base_image'], 100, 100);


		$suc = array('result' =>array('status'=>"success",'user_dtls' => $user_dtls));
		echo  json_encode($suc);
	}
	/***************************************************************************************************************************************/
	public function list_friends(){
		$friendlist = array();
		$user_id = $this->input->post('user_id');
		$page = $this->input->post('page');
		/// variable
		$total_pages="0";
		$user_details = array();
		///////
		$listperpage = 20;
		if(isset($page))
		{
			$page = $this->input->post('page');
			if ($page > 1)
			{
				$start_from = ($page-1) * $listperpage;
			}
			else
			{
				$start_from = 0;
			}
		}
		else
		{
			$start_from = 0;
		}

		$query_total = "SELECT request_id from tbl_request_contact where  request_status = 1 and  (request_to = ".$user_id." or request_from = ".$user_id.")";

		$query_total = $this->db->query($query_total);
		if($query_total->num_rows() > 0)
		{
			$total_record = $query_total->num_rows();
			$total_pages = ceil($total_record/$listperpage);
		}
		///	$friendlist['total_page'] = $total_pages;
		$where = "request_status = 1 and (request_from = ".$user_id."  OR  request_to=".$user_id.")";
		$this->db->where($where);
		$this->db->order_by("request_id", "desc");
		$this->db->limit($listperpage, $start_from);
		$query = $this->db->get('tbl_request_contact');
		if($query->num_rows() >  0){
			foreach ($query->result() as $row){

				if($row->request_from != $user_id){
					$user_details = $this->getuserprofiledetails($row->request_from);
				}else{
					$user_details = $this->getuserprofiledetails($row->request_to);
				}
				$user_details['invitation_status'] = 1;



				$user_details['original_image'] = base_url().'photos/user_image/'.$user_details['base_image'];
				$user_details['thumb_image'] = $this->get_image_user($user_details['base_image'], 100, 100);



				if($user_details['user_type'] == 2){
					$user_details['services'] = $this->get_hd_services($user_details['user_id']);
					$user_details['opening_hours'] = $this->get_opening_hours($user_details['user_id']);
				}else{
					$user_details['services'] = $this->get_model_services($user_details['user_id']);
				}
				$friendlist[] = $user_details;
			}
		}
		$suc = array('result' =>array('status'=>"success",'user_list' => $friendlist,'total_pages'=>$total_pages));
		echo  json_encode($suc);
	}
	/***************************************************************************************************************************************/
	public function add_opening_hours(){
		$opening_hours = $this->input->post('opening_hours');
		$user_id =  $this->input->post('user_id');
		$openinghours_array = json_decode($opening_hours);
		if(is_array($openinghours_array) && count($openinghours_array) > 0){
			$this->db->where('hd_id =', $user_id);
			$this->db->delete('tbl_salon_opening_time');
			foreach($openinghours_array as $key => $value){
				foreach($value as $days => $schedule){
					$opening_hrs = array(
						'hd_id' => $user_id,
						'week_day' => $days,
						'start_time' => $openinghours_array[$key]->$days->start_time,
						'end_time' => $openinghours_array[$key]->$days->end_time,
						'is_closed' => $openinghours_array[$key]->$days->is_closed,
					);
					$this->db->insert('tbl_salon_opening_time',$opening_hrs);
				}
			}

		}
	}
	/***************************************************************************************************************************************/
	public function logout(){
		$user_id 		=  $this->input->post('user_id');
		$device_token 	= $this->input->post('device_token');
		$this->db->where('user_id =', $user_id);
		$this->db->where('device_token	 =', $device_token);
		if($this->db->delete('user_device_token')){
			echo '{"result":{"status":"success","message":"user logged out successfully"}}';
		}else{
			echo '{"result":{"status":"error","message":"user not logged out successfully"}}';
		}
	}
	/***************************************************************************************************************************************/
	public function get_opening_hours($user_id){
		$opening_hrs_array = array();
		$this->db->where('hd_id',$user_id);
		$query_opening_hrs = $this->db->get('tbl_salon_opening_time');
		$opening_hrs_array = array();
		if($query_opening_hrs->num_rows() >  0){
			foreach ($query_opening_hrs->result() as $rows){
				$opening_hrs_array[] = array('day' => $rows->week_day,'start_time' => $rows->start_time,'end_time' => $rows->end_time,'is_closed' => $rows->is_closed);
			}
		}
		return $opening_hrs_array;
	}
	/***************************************************************************************************************************************/
	public function forget_pass(){
		$this->load->helper('url');
		$user_email = $this->security->xss_clean($this->input->post('user_email'));
		//$user_email = 'hd3@gmail.com';
		if(isset($user_email) && $user_email != "")
		{
			$this->db->where('user_email =', $user_email);
			$query = $this->db->get('tbl_users');
			if($query->num_rows() >0)
			{
				$password = $this->randomPrefix(6);
				$data = array(
					'user_password'=>$password,
					'is_otp'	=> '1',
					'otp_changed' => '0'
					);
					$this->db->where('user_email', $user_email);
					if($this->db->update('tbl_users', $data)=='1')
					{
						$this->load->model('mail_function');
						$body =  'Please use '.$password.' as your newpassword to login to Guineapig account and change the password when you logged in to your account';
						$chk = $this->mail_function->sendmail('arvind@appster.com.au',"Guineapig New Password Detail",$body);
						if($chk == '1')
						{
							echo '{"result":{"status":"success","message":"An email has been sent to your respective email address."}}';
						}else{
							echo '{"result":{"status":"error","message":"some issue in mail sent"}}';
						}
					}
			}
			else
			{
				echo '{"result":{"status":"error","message":"Email address does not exists."}}';
			}
		}
		else
		{
			echo '{"result":{"status":"error","message":"Please enter email address."}}';
		}

	}
	/*******************************************************************************************/
	public function randomPrefix($length)
	{
		$random= "";
			
		srand((double)microtime()*1000000);
		$data = "abcdefghijklmnopqrstuvwxyz1234567890";
		for($i = 0; $i < $length; $i++)
		{
			$random .= substr($data, (rand()%(strlen($data))), 1);
		}
		return $random;
	}
	/*******************************************************************************************/
	public function change_otp(){
		$user_id 		=  $this->input->post('user_id');
		$newpass 	= $this->input->post('password');
		$data = array(
					'user_password'=>$newpass,
					'otp_changed' => '1'
					);
					$this->db->where('user_id', $user_id);
					$this->db->update('tbl_users', $data);
					echo '{"result":{"status":"success","message":"Password updated successfully"}}';
	}
	/*******************************************************************************************/
	public function get_product_list(){
		$query = $this->db->get('tbl_product_list');
		$procucts = array();
		if($query->num_rows() >0)
		{
			foreach($query->result() as $row){
				$procucts[] = array(
						'procuct_id' => $row->product_id , 
						'product_key' =>$row->product_key,
						'product_name' =>$row->product_name,
						'product_description' => $row->product_description,
						'product_validity' => $row->product_validity
				);
			}
		}
		$suc = array('result' => array('status'=>"success",'product_list' => $procucts));
		echo  json_encode($suc);
	}
	/*******************************************************************************************/
	public function purchase_product(){
		$user_id = $this->security->xss_clean($this->input->post('user_id'));
		$product_id = $this->security->xss_clean($this->input->post('product_id'));
		$order_ref = $this->security->xss_clean($this->input->post('order_ref_id'));
		//$order_receipt = $this->security->xss_clean($this->input->post('order_receipt'));
		// get product_details
		$this->db->where('procuct_id =', $product_id);
		$query = $this->db->get('tbl_product_list');
		$productdtls = $query->result_array();
		$validity = $productdtls[0]['product_validity'];
		// get any active subscription
		//$this->db->where('procuct_id =', $product_id);
		$this->db->where('expairy_date >=', date('Y-m-d'));
		$this->db->where('user_id =', $user_id);
		$this->db->order_by("subscription_id", "desc");
		$this->db->limit(1, 0);
		$query = $this->db->get('tbl_subscription');
		if($query->num_rows() > 0){
			$subscription_dtls = $query->result_array();
			$date = strtotime($subscription_dtls[0]['expairy_date']);
			$exp_date = date('Y-m-d',strtotime('+'.$validity. 'days',$date));
		}else{
			$date = strtotime(date('Y-m-d'));
			$exp_date = date('Y-m-d',strtotime('+'.$validity. 'days',$date));
		}
		$data = array(
			'user_id' => $user_id,
			'product_id' => $product_id,
			'purchase_date' => date('Y-m-d'),
			'expairy_date'  => $exp_date,
			'order_ref_id' => $order_ref
		);
		if($this->db->insert('tbl_subscription',$data)){
			echo '{"result":{"status":"success","message":"Payment details updated successfully"}}';
		}else{
			echo '{"result":{"status":"failure","message":"Some error occoured"}}';
		}
	}
	/*******************************************************************************************/
	public function list_friend_request_sent(){
		$this->load->helper('url');
		$user_id = $this->input->post('user_id');
		$page = $this->input->post('page');
		$listperpage = 20;

		$total_pages="0";
		$user_list = array();


		// start paging related task
		if(isset($page))
		{
			$page = $this->input->post('page');
			if ($page > 1)
			{
				$start_from = ($page-1) * $listperpage;
			}
			else
			{
				$start_from = 0;
			}
		}else{
			$start_from = 0;
		}
		$total = "0";
		// get friend request list
		$this->db->where('request_status =', 0);
		$this->db->where('request_from =', $user_id);
		$query = $this->db->get('tbl_request_contact');
		if($query->num_rows() >  0){
			$total = $query->num_rows();
			$total_pages = ceil($total/$listperpage);
		}
		$this->db->where('request_status =', 0);
		$this->db->where('request_from =', $user_id);
		$this->db->limit($listperpage, $start_from);
		$query = $this->db->get('tbl_request_contact');
		if($query->num_rows() >  0){

			foreach ($query->result() as $row){
				$user_details = $this->getuserprofiledetails($row->request_to);


				$user_details['original_image'] = base_url().'photos/user_image/'.$user_details['base_image'];
				$user_details['thumb_image'] = $this->get_image_user($user_details['base_image'], 100, 100);


				if($user_details['user_type'] == 2){
					$user_details['services'] = $this->get_hd_services($user_details['user_id']);
					$user_details['opening_hours'] = $this->get_opening_hours($user_details['user_id']);
				}else{
					$user_details['services'] = $this->get_model_services($user_details['user_id']);
				}

				$user_details['invitation_id'] = $row->request_id;
				$user_list[] = $user_details;
			}
		}
		$suc = array('result' =>array('status'=>"success",'user_list' => $user_list , 'total_pages' => $total_pages));
		echo  json_encode($suc);
	}
	/*******************************************************************************************/
	public function	list_friend_request_received(){
		$this->load->helper('url');
		$user_id = $this->input->post('user_id');
		$page = $this->input->post('page');
		$listperpage = 20;


		$user_list = array();


		// start paging related task
		if(isset($page))
		{
			$page = $this->input->post('page');
			if ($page > 1)
			{
				$start_from = ($page-1) * $listperpage;
			}
			else
			{
				$start_from = 0;
			}
		}else{
			$start_from = 0;
		}
		$total_pages = "0";
		// get friend request list
		$this->db->where('request_status =', 0);
		$this->db->where('request_to =', $user_id);
		$query = $this->db->get('tbl_request_contact');
		if($query->num_rows() >  0){
			$total = $query->num_rows();
			$total_pages = ceil($total/$listperpage);
		}
		$this->db->where('request_status =', 0);
		$this->db->where('request_to =', $user_id);
		$this->db->limit($listperpage, $start_from);
		$query = $this->db->get('tbl_request_contact');
		if($query->num_rows() >  0){

			foreach ($query->result() as $row){
				$user_details = $this->getuserprofiledetails($row->request_from);

				$user_details['original_image'] = base_url().'photos/user_image/'.$user_details['base_image'];
				$user_details['thumb_image'] = $this->get_image_user($user_details['base_image'], 100, 100);


				if($user_details['user_type'] == 2){
					$user_details['services'] = $this->get_hd_services($user_details['user_id']);
					$user_details['opening_hours'] = $this->get_opening_hours($user_details['user_id']);
				}else{
					$user_details['services'] = $this->get_model_services($user_details['user_id']);
				}

				$user_details['invitation_id'] = $row->request_id;
				$user_list[] = $user_details;
			}
		}
		$suc = array('result' =>array('status'=>"success",'user_list' => $user_list , 'total_pages' => $total_pages));
		echo  json_encode($suc);
	}
	public function user_subscription_details($user_id){
		$week = date('W');
		$year = date('Y');
		$monday    = date('Y-m-d', strtotime("$year-W$week-1"));
		$sunday    = date('Y-m-d', strtotime("$year-W$week-7"));
		/// check if the user is subscribed or not
		$is_subscribed = 0;
		$this->db->where('expairy_date >=', date('Y-m-d'));
		$this->db->where('user_id =', $user_id);
		$this->db->order_by("subscription_id", "desc");
		$this->db->limit(1, 0);
		$query = $this->db->get('tbl_subscription');
		if($query->num_rows() > 0){
			$is_subscribed = 1;
		}
		/// get total number of request sent by user in current week
		$this->db->where('request_from =', $user_id);
		$this->db->where("date(request_date) BETWEEN $monday AND $sunday");
		$query = $this->db->get('tbl_request_contact');
		$num_request = $query->num_rows();
		$sub_details = array('is_subscribed' => $is_subscribed,'total_request' => $num_request);
		return $sub_details;
	}



	public function unfriend()
	{
		$this->load->helper('url');
		$user_id = $this->input->post('user_id');
		$friend_id = $this->input->post('friend_id');

		$this->db->where("request_status =","1");
		$this->db->where("user_id =",$user_id);
		$this->db->where("friend_id =",$friend_id);
		$this->db->or_where("friend_id =",$user_id);
		$this->db->where("user_id =",$friend_id);
		$query = $this->db->get("tbl_request_contact");

		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $row)
			{
				$this->db->delete("tbl_request_contact",array("id"=>$row->id));
			}

			$suc = array('result' =>array('status'=>"success",'message'=>"Unfriend successfully"));
			echo  json_encode($suc);

		}
		else
		{
			$suc = array('result' =>array('status'=>"error",'message'=>"No longer friends"));
			echo  json_encode($suc);
		}
	}

}